<?php

namespace Drupal\Tests\migrate_json_example\Kernel;

use Drupal\Tests\migrate_drupal\Kernel\MigrateDrupalTestBase;

/**
 * Tests migrate_json_example migrations.
 *
 * @group migrate_plus
 */
class MigrateJsonExampleTest extends MigrateDrupalTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'file',
    'text',
    'migrate_plus',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['node']);
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);

    // Install the module via installer to trigger hook_install.
    \Drupal::service('module_installer')->install(['migrate_json_example']);
    $this->installConfig(['migrate_json_example']);
  }

  /**
   * Tests the results of "migrate_json_example" migrations.
   */
  public function testMigrations() {
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $this->assertCount(0, $node_storage->loadMultiple());
    // Execute "product" migration from 'migrate_json_example' module.
    $this->executeMigration('product');
    $this->assertCount(2, $node_storage->loadMultiple());
  }

}
